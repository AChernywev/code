//
//  UIImage+Remote.h
//  MobileAvenger
//
//  Created by Alexandr Chernyshev on 27.02.14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Remote)

- (void)setImageWithURLString:(NSString *)urlString withActivity:(BOOL)activity defaultImage:(UIImage *)defaultImage;
@end
