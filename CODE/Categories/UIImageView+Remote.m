//
//  UIImage+Remote.m
//  MobileAvenger
//
//  Created by Alexandr Chernyshev on 27.02.14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "UIImageView+Remote.h"

#import <objc/runtime.h>

static char * const kActivityViewKey    = "ActivityView";
static char * const kRemoteImageLinkKey = "RemoteImageLink";

@interface UIImageView ()
@property (nonatomic, strong) UIActivityIndicatorView * activityView;
@property (nonatomic, strong) NSString *                remoteImageLink;

@end

@implementation UIImageView (Remote)

#pragma mark - property setters and getters
- (UIActivityIndicatorView *)activityView
{
    return objc_getAssociatedObject(self, &kActivityViewKey);
}

- (void)setActivityView:(UIActivityIndicatorView *)activityView
{
    objc_setAssociatedObject(self, &kActivityViewKey, activityView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)remoteImageLink
{
    return objc_getAssociatedObject(self, &kRemoteImageLinkKey);
}

- (void)setRemoteImageLink:(NSString *)remoteImageLink
{
    objc_setAssociatedObject(self, &kRemoteImageLinkKey, remoteImageLink, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - public methods
- (void)setImageWithURLString:(NSString *)urlString withActivity:(BOOL)activity defaultImage:(UIImage *)defaultImage
{
    self.remoteImageLink = urlString;
    self.image = defaultImage;
    
    if (activity) {
        if (!self.activityView) {
            self.activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            self.activityView.autoresizingMask = UIViewAutoresizingNone;
            [self addSubview:self.activityView];
        }
        self.activityView.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
        [self.activityView startAnimating];
    }
    else {
        if (self.activityView) {
            [self.activityView stopAnimating];
            [self.activityView removeFromSuperview];
            self.activityView = nil;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    [[Model shared].imageLoader loadImageFromURLString:urlString withCompletion:^(NSString *imageURLString, UIImage *image) {
        if ([weakSelf.remoteImageLink isEqualToString:imageURLString]) {
            [weakSelf.activityView stopAnimating];
            [weakSelf.activityView removeFromSuperview];
            weakSelf.activityView = nil;
            
            if(image) {
                weakSelf.image = image;
            }
        }
    }];
}

@end
