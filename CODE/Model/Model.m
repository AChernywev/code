//
//  Model.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "Model.h"

static id sharedInstance = nil;

@implementation Model

#pragma mark - singleton methods
+ (instancetype)shared
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc]init];
    });
    return sharedInstance;
}

+ (void)dispose
{
    sharedInstance = nil;
}

#pragma mark - lifecycle
- (id)init
{
    self = [super init];
    if(self) {
        _imageLoader = [[ImageLoader alloc]init];
    }
    return self;
}

- (void)dealloc
{
}
@end
