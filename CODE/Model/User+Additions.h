//
//  User+Additions.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "User.h"

#import "InstagramRequest.h"

@interface User (Additions)

+ (InstagramRequest *)searchUsersWithString:(NSString *)username completion:(void(^)(NSArray *users, NSError *error))completion;
- (InstagramRequest *)updateUserInfoWithCompletion:(void(^)(NSError *error))completion;
- (InstagramRequest *)loadUserRecentImagesWithNextID:(NSString *)nextID
                                          completion:(void(^)(NSArray *images, NSString *nextID, NSError *error))completion;
@end
