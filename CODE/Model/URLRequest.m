//
//  URLRequest.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "URLRequest.h"

@implementation URLRequest

#pragma mark - default settings
+ (NSURLRequestCachePolicy) defaultCachePolicy
{
    return NSURLRequestReloadIgnoringLocalCacheData;
}


+ (NSTimeInterval) defaultTimeoutInterval
{
    return 60.0;
}

#pragma mark class method-style initializers
+ (instancetype) requestWithURLString:(NSString *)urlString
              requestMethod:(RequestMethod)requestMethod
            responseHandler:(RequestResponseHandler)responseHandler
{
    return [[[self class] alloc] initWithURLString:urlString
                                     requestMethod:requestMethod
                                   responseHandler:responseHandler];
}

#pragma mark - initialization methods
- (instancetype) initWithURLString:(NSString *)urlString
           requestMethod:(RequestMethod)requestMethod
         responseHandler:(RequestResponseHandler)responseHandler
{
    self = [super init];
    
    if (self != nil)
    {
        _responseHandler = [responseHandler copy];
        
        _request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:urlString]
                                                cachePolicy: [[self class] defaultCachePolicy]
                                            timeoutInterval: [[self class] defaultTimeoutInterval]];
        _request.HTTPMethod = [self httpMethodForRequestMethod:requestMethod];
    }
    return self;
}

#pragma mark dealloc
- (void) dealloc
{
    [self cancelConnectionAndCleanData];
}

#pragma mark - Cleanup methods

- (void) cancelConnectionAndCleanData
{
    [_connection cancel];
    _expectedContentLength  = 0;
    self.progressHandler    = nil;
    _connection             = nil;
    _downloadedData         = nil;
    _request                = nil;
    _responseHandler        = nil;
}


#pragma mark - working methods
- (NSString *)httpMethodForRequestMethod:(RequestMethod)requestMethod
{
    NSString *method = nil;
    switch (requestMethod) {
        case RequestMethodGet: {
            method = @"GET";
        }
            break;
        case RequestMethodPost: {
            method = @"POST";
        }
            break;
        case RequestMethodDelete: {
            method = @"DELETE";
        }
            break;
        case RequestMethodPut: {
            method = @"PUT";
        }
            break;
            
        default: {
            NSAssert(NO, @"Unknown http method");
        }
            break;
    }
    return method;
}

- (void) startConnection
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        if (_connection == nil)
        {
            _connection = [[NSURLConnection alloc] initWithRequest: _request
                                                          delegate: self];
            _downloadedData = [NSMutableData data];
            [_connection start];
        }
    });
}

- (void) cancelConnection
{
    [self cancelConnectionAndCleanData];
}

- (void)setBodyParamsJSONSerialize:(NSDictionary *)params
{
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&jsonSerializationError];
    
    if(!jsonSerializationError) {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    [_request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [_request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [_request setHTTPBody: jsonData];
}

- (void) failWithError: (NSError *) error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_responseHandler != nil) {
            _responseHandler(nil, error);
        }
        [self cancelConnectionAndCleanData];
    });
}

- (void) succeedWithResponse: (id) response
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_responseHandler != nil) {
            _responseHandler(response, nil);
        }
        [self cancelConnectionAndCleanData];
    });
}

- (void) preprocessResponse: (id) response
{
    [self succeedWithResponse:response];
}

#pragma mark - <NSURLConnectionDelegate>
- (void)connection: (NSURLConnection *) connection
  didFailWithError: (NSError *)         error
{
    [self failWithError: error];
}

#pragma mark - <NSURLConnectionDataDelegate>
- (void)connection: (NSURLConnection *) connection
didReceiveResponse: (NSURLResponse   *) response
{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    _httpStatusCode = [httpResponse statusCode];
    if(response.expectedContentLength != -1) {
        _expectedContentLength = response.expectedContentLength;
        if(self.progressHandler) {
            self.progressHandler(0.0);
        }
    }
    else {
        self.progressHandler = nil;
    }
    [_downloadedData setLength: 0];
}

- (void) connection: (NSURLConnection *) connection
     didReceiveData: (NSData *)          data
{
    [_downloadedData appendData: data];
    
    if(self.progressHandler) {
        float progress = (float)_downloadedData.length / (float)_expectedContentLength;
        self.progressHandler(progress);
    }
}

- (void) connectionDidFinishLoading: (NSURLConnection *) connection
{
    if (_httpStatusCode <= 400) {
        [self preprocessResponse:_downloadedData];
    }
    else {
        NSError *error = [NSError errorWithDomain:@"URLRequestDomain" code:_httpStatusCode userInfo:@{NSLocalizedDescriptionKey: @"Сервер временно недоступен"}];
        [self failWithError: error];
    }
}

@end
