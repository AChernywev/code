//
//  PhotoPageRenderer.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "PhotoPageRenderer.h"

@interface PhotoPageRenderer()
@property (nonatomic, strong) UIImage *image;

@end

@implementation PhotoPageRenderer

#pragma mark - initialization
- (instancetype)initWithImage:(UIImage *)image
{
    self = [super init];
    if(self) {
        self.image = image;
    }
    return self;
}

-(NSInteger)numberOfPages
{
  return 1;
}

- (void)drawPageAtIndex:(NSInteger)pageIndex inRect:(CGRect)printableRect
{
  if(self.image) {
      CGSize originalSize = self.image.size;
      CGSize desiredSize = printableRect.size;
      CGFloat widthFactor = originalSize.width / desiredSize.width;
      CGFloat heightFactor = originalSize.height / desiredSize.height;
      CGFloat factor = MAX(widthFactor, heightFactor);
      
      CGSize newSize = CGSizeMake(originalSize.width / factor, originalSize.height / factor);
      CGRect drawingRect = CGRectZero;
      drawingRect.origin.x = (printableRect.size.width - newSize.width) / 2.0f;
      drawingRect.origin.y = (printableRect.size.height - newSize.height) / 2.0f;
      drawingRect.size = newSize;
      
      [self.image drawInRect:drawingRect];
  }
  else {
      NSLog(@"No image to draw!");
  }
}

@end
