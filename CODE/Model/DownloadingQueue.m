//
//  DownloadingQueue.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "DownloadingQueue.h"

#import "URLRequest.h"

@implementation DownloadingQueue
{
    NSMutableArray *    _objectsQueue;
    NSMutableSet *      _loadingObjects;
}

#pragma mark - initialization
- (id)init
{
    self = [super init];
    if(self) {
        _objectsQueue   = [NSMutableArray array];
        _loadingObjects = [NSMutableSet set];
    }
    return self;
}

#pragma mark - public methods
- (void)addObjectsToQueue:(NSArray *)objects
{
    for (NSString *object in objects) {
        [self addObjectToQueue:object];
    }
}

- (void)addObjectToQueue:(id)object;
{
    if (object){
        if(![_loadingObjects containsObject:object]) {
            if([_objectsQueue containsObject:object]) {
                [_objectsQueue removeObject:object];
            }
            [_objectsQueue insertObject:object atIndex:0];
            [self starLoadingNextObject];
        }
    }
}

- (BOOL)hasObjectsInQueue
{
    return (_loadingObjects.count || _objectsQueue.count);
}

#pragma mark - working methods
- (void)starLoadingNextObject
{
    NSInteger maxNumOfDownloads = [self.dataSource numberOfSimultaneousDownloads];
    if(_objectsQueue.count && (_loadingObjects.count < maxNumOfDownloads)) {
        id object = [_objectsQueue objectAtIndex:0];
        
        [_loadingObjects addObject:object];
        [_objectsQueue removeObject:object];
        [self startLoadingObject:object];
    }
}

- (void)startLoadingObject:(id)object
{
    __weak DownloadingQueue *weakSelf = self;
    URLRequest *request = [URLRequest requestWithURLString:[self.dataSource linkFromQueueObject:object]
                                       requestMethod:RequestMethodGet
                                     responseHandler:^(id response, NSError *error) {
                                         
                                         [weakSelf finishLoadingObject:object];
                                         
                                         if(error) {
                                             if([weakSelf.delegate respondsToSelector:@selector(queue:didFailedLoadObject:withError:)]) {
                                                 [weakSelf.delegate queue:weakSelf didFailedLoadObject:object withError:error];
                                             }
                                         }
                                         else {
                                             if([weakSelf.delegate respondsToSelector:@selector(queue: didLoadObject:withResponse:)]) {
                                                 [weakSelf.delegate queue:weakSelf didLoadObject:object withResponse:response];
                                             }
                                         }
                                     }
                           ];
    request.progressHandler = ^(float progress) {
        if([weakSelf.delegate respondsToSelector:@selector(queue: downloadingProgressChanged:forObject:)]) {
            [weakSelf.delegate queue:weakSelf downloadingProgressChanged:progress forObject:object];
        }
    };
    [request startConnection];
}

- (void)finishLoadingObject:(id)object
{
    [_loadingObjects removeObject:object];
    [self starLoadingNextObject];
}
@end
