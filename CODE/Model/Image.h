//
//  Image.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/7/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "BaseObject.h"

@interface Image : BaseObject
@property (nonatomic, strong) NSString *imageID;
@property (nonatomic, strong) NSString *lowResolutionURLString;
@property (nonatomic, strong) NSString *thumbnailURLString;
@property (nonatomic, strong) NSString *standardResolutionURLString;
@property (nonatomic, assign) NSInteger likesCount;
@property (nonatomic, assign) NSInteger commentsCount;

@end

#import "Image+Additions.h"
