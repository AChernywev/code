//
//  User.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "BaseObject.h"
#import "Image.h"

@interface User : BaseObject
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *profilePictureURLString;
@property (nonatomic, strong) NSString *bio;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, assign) NSInteger postsCount;
@property (nonatomic, assign) NSInteger folowsCount;
@property (nonatomic, assign) NSInteger folowedByCount;

@end

#import "User+Additions.h"
