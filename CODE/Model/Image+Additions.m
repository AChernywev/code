//
//  Image+Additions.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/7/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "Image+Additions.h"

@implementation Image (Additions)

#pragma mark - Base Object methods
- (void)updateWithDictionary:(NSDictionary *)dictionary
{
    self.imageID = JSON_TO_STRING(dictionary[@"id"]);
    NSDictionary *imagesDictionary = JSON_TO_DICTIONARY(dictionary[@"images"]);
    self.lowResolutionURLString = JSON_TO_STRING(JSON_TO_DICTIONARY(imagesDictionary[@"low_resolution"])[@"url"]);
    self.thumbnailURLString = JSON_TO_STRING(JSON_TO_DICTIONARY(imagesDictionary[@"thumbnail"])[@"url"]);
    self.standardResolutionURLString = JSON_TO_STRING(JSON_TO_DICTIONARY(imagesDictionary[@"standard_resolution"])[@"url"]);
    self.likesCount = JSON_TO_INTEGER(JSON_TO_DICTIONARY(dictionary[@"likes"])[@"count"]);
    self.commentsCount = JSON_TO_INTEGER(JSON_TO_DICTIONARY(dictionary[@"comments"])[@"count"]);
}
@end
