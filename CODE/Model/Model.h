//
//  Model.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "ImageLoader.h"
#import "User.h"

@interface Model : NSObject
@property (nonatomic, readonly, strong) ImageLoader *       imageLoader;

//singleton
+ (instancetype)shared;
+ (void)dispose;
@end
