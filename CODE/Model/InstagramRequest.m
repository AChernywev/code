//
//  InstagramRequest.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "InstagramRequest.h"

static NSString * const kClientIDString     = @"7795913c97194400875c6f2bdd8916b0";
static NSInteger  const kEmptyServerRespond = 100000;
static NSInteger  const kUnknownRepondType  = 100001;
static NSInteger  const kSuccessStatusCode  = 200;

@implementation InstagramRequest

#pragma mark - URL Request settings
+ (NSURLRequestCachePolicy) defaultCachePolicy
{
    return NSURLRequestReloadIgnoringLocalCacheData;
}

+ (NSTimeInterval) defaultTimeoutInterval
{
    return 20.0;
}

#pragma mark - Class methods
+ (NSString *)baseServerURL
{
    return @"https://api.instagram.com/v1";
}

+ (NSString *)absoluteAddressFromRelative:(NSString *)urlString
{
    NSString *requestString = nil;
    if([urlString rangeOfString:[[self class] baseServerURL]].location == NSNotFound) {
        if([urlString hasPrefix:@"/"]) {
            requestString = [NSString stringWithFormat:@"%@%@",[[self class] baseServerURL], urlString];
        }
        else {
            requestString = [NSString stringWithFormat:@"%@/%@",[[self class] baseServerURL], urlString];
        }
    }
    else {
        requestString = urlString;
    }
    return requestString;
}

#pragma mark class method-style initializers
+ (instancetype) requestWithURLString:(NSString *)urlString
                        requestMethod:(RequestMethod)requestMethod
                      responseHandler:(RequestResponseHandler)responseHandler
{
    return [self requestWithURLString:urlString
                        requestMethod:requestMethod
                          useClientID:NO
                      responseHandler:responseHandler];
}

+ (instancetype) requestWithURLString: (NSString *) urlString
                        requestMethod: (RequestMethod)requestMethod
                          useClientID: (BOOL)useClientID
                      responseHandler: (RequestResponseHandler) responseHandler
{
    return [[[self class] alloc]initWithURLString:urlString
                                    requestMethod:requestMethod
                                      useClientID:useClientID
                                  responseHandler:responseHandler];
}

#pragma mark - initialization methods
- (instancetype)initWithURLString:(NSString *)urlString
                    requestMethod:(RequestMethod)requestMethod
                  responseHandler:(RequestResponseHandler)responseHandler
{
    return [self initWithURLString:urlString requestMethod:requestMethod useClientID:NO responseHandler:responseHandler];
}

- (instancetype)  initWithURLString: (NSString *) urlString
                      requestMethod: (RequestMethod)requestMethod
                        useClientID: (BOOL)useClientID
                    responseHandler: (RequestResponseHandler) responseHandler
{
    NSString *resultString = urlString;
    if(useClientID) {
        NSRange range = [urlString rangeOfString:@"?"];
        if(range.location != NSNotFound) {
            resultString = [resultString stringByAppendingFormat:@"&client_id=%@",kClientIDString];
        }
        else {
            resultString = [resultString stringByAppendingFormat:@"?client_id=%@",kClientIDString];
        }
    }
    self = [super initWithURLString:[[self class] absoluteAddressFromRelative:resultString] requestMethod:requestMethod responseHandler:responseHandler];
    if (self) {
    }
    return self;
}

#pragma mark - customization methods
- (void) preprocessResponse: (id) response
{
    if(response) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:response options:0 error:nil];
        if(dict && [dict isKindOfClass:[NSDictionary class]]) {
            NSDictionary *metaDictionary = [dict objectForKey:@"meta"];
            NSInteger statusCode = [[metaDictionary objectForKey:@"code"] integerValue];
            if(statusCode != kSuccessStatusCode) {
                [self failWithError:[self errorWithType:[metaDictionary objectForKey:@"error_type"] message:[metaDictionary objectForKey:@"error_message"]]];
            }
            else {
                [self succeedWithResponse:dict];
            }
        }
        else {
            [self failWithError:[self errorWithCode:kUnknownRepondType message:@"Сервер временно недоступен"]];
        }
    }
    else {
        [self failWithError:[self errorWithCode:kEmptyServerRespond message:@"Не удалось получить ответ от сервера"]];
    }
}

#pragma mark - working methods
- (NSError *)errorWithType:(NSString *)errorType message:(NSString *)message
{
    if ([errorType isEqualToString:@"APINotAllowedError"]) {
        message = @"Пользователь установил настройки конфиденциальности";
    }
    NSError *error = [NSError errorWithDomain:[[self class] baseServerURL] code:0 userInfo:@{NSLocalizedDescriptionKey: message}];
    return error;
}

- (NSError *)errorWithCode:(NSInteger)code message:(NSString *)message
{
    NSError *error = [NSError errorWithDomain:[[self class] baseServerURL] code:code userInfo:@{NSLocalizedDescriptionKey: message}];
    return error;
}
@end
