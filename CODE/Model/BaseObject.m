//
//  BaseObject.c
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "BaseObject.h"

@implementation BaseObject

#pragma mark - class methods
+ (NSArray *)objectsArrayFromDictionaryArray:(NSArray *)dictionaryArray
{
    NSMutableArray *resultArray = [NSMutableArray arrayWithCapacity:dictionaryArray.count];
    for(NSDictionary *dict in dictionaryArray) {
        [resultArray addObject:[[self class] objectWithDictionary:dict]];
    }
    return resultArray;
}

+ (instancetype)objectWithDictionary:(NSDictionary *)objectDictionary
{
    BaseObject *object = [[[self class] alloc]init];
    [object updateWithDictionary:objectDictionary];
    return object;
}

#pragma mark - public methods
- (void)updateWithDictionary:(NSDictionary *)dictionary
{
}

@end
