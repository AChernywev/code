//
//  ImageLoader.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//
#import <Foundation/Foundation.h>

typedef void (^ImageLoaderCompletionBlock) (NSString *imageURLString, UIImage *image);

@interface ImageLoader : NSObject

- (void)loadImageFromURLString:(NSString *)urlString withCompletion:(ImageLoaderCompletionBlock)completion;
@end
