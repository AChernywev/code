//
//  CommonUserProtocol.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

@interface BaseObject : NSObject

+ (NSArray *)objectsArrayFromDictionaryArray:(NSArray *)dictionaryArray;
+ (instancetype)objectWithDictionary:(NSDictionary *)objectDictionary;
- (void)updateWithDictionary:(NSDictionary *)dictionary;
@end
