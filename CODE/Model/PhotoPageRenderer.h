//
//  PhotoPageRenderer.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//


@interface PhotoPageRenderer : UIPrintPageRenderer

- (instancetype)initWithImage:(UIImage *)image;
@end
