//
//  User+Additions.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "User+Additions.h"

#import "InstagramRequest.h"

@implementation User (Additions)

+ (InstagramRequest *)searchUsersWithString:(NSString *)username completion:(void(^)(NSArray *users, NSError *error))completion
{
    NSString *requestString = [NSString stringWithFormat:@"users/search?q=%@", username];
    InstagramRequest *request = [InstagramRequest requestWithURLString:requestString
                                                         requestMethod:RequestMethodGet
                                                           useClientID:YES
                                                       responseHandler:^(id response, NSError *error) {
                                                           NSArray *users = nil;
                                                           if (!error) {
                                                               users = [User objectsArrayFromDictionaryArray:JSON_TO_ARRAY([response objectForKey:@"data"])];
                                                               if(JSON_TO_DICTIONARY(response[@"pagination"])) {
                                                                   NSLog(@"Has Paging In Users");
                                                               }
                                                               else {
                                                                   NSLog(@"Has not Paging In Users");
                                                               }
                                                           }
                                                           if(completion) {
                                                               completion(users, error);
                                                           }
                                                       }
                                 ];
    [request startConnection];
    return request;
}

- (InstagramRequest *)updateUserInfoWithCompletion:(void(^)(NSError *error))completion
{
    NSString *requestString = [NSString stringWithFormat:@"/users/%@", self.userID];
    __weak typeof (self) weakSelf = self;
    InstagramRequest *request = [InstagramRequest requestWithURLString:requestString
                                                         requestMethod:RequestMethodGet
                                                           useClientID:YES
                                                       responseHandler:^(id response, NSError *error) {
                                                           if (!error) {
                                                               [weakSelf updateWithDictionary:JSON_TO_DICTIONARY(response[@"data"])];
                                                           }
                                                           if(completion) {
                                                               completion(error);
                                                           }
                                                       }
                                 ];
    [request startConnection];
    return request;
}

- (InstagramRequest *)loadUserRecentImagesWithNextID:(NSString *)nextID
                         completion:(void(^)(NSArray *images, NSString *nextID, NSError *error))completion
{
    NSString *requestString = [NSString stringWithFormat:@"/users/%@/media/recent?count=33", self.userID];
    if(nextID.length > 0) {
        requestString = [requestString stringByAppendingFormat:@"&max_id=%@",nextID];
    }
    InstagramRequest *request = [InstagramRequest requestWithURLString:requestString
                                                         requestMethod:RequestMethodGet
                                                           useClientID:YES
                                                       responseHandler:^(id response, NSError *error) {
                                                           NSArray *images = nil;
                                                           NSString *newMaxID = nil;
                                                           if (!error) {
                                                               images = [Image objectsArrayFromDictionaryArray:JSON_TO_ARRAY([response objectForKey:@"data"])];
                                                               NSDictionary *paginDict = JSON_TO_DICTIONARY(response[@"pagination"]);
                                                               if(paginDict.count) {
                                                                   NSString *nextMaxID = JSON_TO_STRING(paginDict[@"next_max_id"]);
                                                                   if(![nextMaxID isEqualToString:nextID]) {
                                                                       newMaxID = nextMaxID;
                                                                   }
                                                               }
                                                           }
                                                           if(completion) {
                                                               completion(images, newMaxID, error);
                                                           }
                                                       }
                                 ];
    [request startConnection];
    return request;
}

#pragma mark - Base Object methods
- (void)updateWithDictionary:(NSDictionary *)dictionary
{
    self.userID = JSON_TO_STRING(dictionary[@"id"]);
    self.username = JSON_TO_STRING(dictionary[@"username"]);
    self.fullname = JSON_TO_STRING(dictionary[@"full_name"]);
    self.profilePictureURLString = JSON_TO_STRING(dictionary[@"profile_picture"]);
    self.bio = JSON_TO_STRING(dictionary[@"bio"]);
    self.website = JSON_TO_STRING(dictionary[@"website"]);
    NSDictionary *countDict = JSON_TO_DICTIONARY(dictionary[@"counts"]);
    self.postsCount = JSON_TO_INTEGER(countDict[@"media"]);
    self.folowsCount = JSON_TO_INTEGER(countDict[@"follows"]);;
    self.folowedByCount = JSON_TO_INTEGER(countDict[@"followed_by"]);;
}
@end
