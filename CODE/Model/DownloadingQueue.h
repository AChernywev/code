//
//  DownloadingQueue.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DownloadingQueue;

@protocol DownloadingQueueDataSource <NSObject>

@required
- (NSInteger)numberOfSimultaneousDownloads;
- (NSString *)linkFromQueueObject:(id)object;
@end


@protocol DownloadingQueueDelegate <NSObject>

- (void)queue:(DownloadingQueue *)queue didFailedLoadObject:(id)object withError:(NSError *)error;
- (void)queue:(DownloadingQueue *)queue didLoadObject:(id)object withResponse:(id)response;
- (void)queue:(DownloadingQueue *)queue downloadingProgressChanged:(float)progress forObject:(id)object;
@end

@interface DownloadingQueue : NSObject
@property (nonatomic, weak) id<DownloadingQueueDelegate>delegate;
@property (nonatomic, weak) id<DownloadingQueueDataSource>dataSource;

- (void)addObjectsToQueue:(NSArray *)objects;
- (void)addObjectToQueue:(id)object;

- (BOOL)hasObjectsInQueue;
@end
