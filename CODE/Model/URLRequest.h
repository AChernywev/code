//
//  URLRequest.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    RequestMethodPost,
    RequestMethodGet,
    RequestMethodPut,
    RequestMethodDelete
}
RequestMethod;

typedef void (^RequestResponseHandler)(id response, NSError *error);
typedef void (^ProgressHandler)(float progress);

@interface URLRequest : NSObject <NSURLConnectionDataDelegate, NSURLConnectionDelegate>
{
@protected
    NSMutableURLRequest *   _request;
    NSURLConnection     *   _connection;
    NSMutableData       *   _downloadedData;
    RequestResponseHandler  _responseHandler;
    long long               _expectedContentLength;
    NSInteger               _httpStatusCode;
}
@property (strong, nonatomic) NSString * requestID;
@property (copy, nonatomic)   ProgressHandler progressHandler;

#pragma mark initializer methods
- (instancetype)  initWithURLString: (NSString *) urlString
            requestMethod: (RequestMethod)requestMethod
          responseHandler: (RequestResponseHandler) responseHandler NS_RETURNS_RETAINED;

#pragma mark class method-style initializers
+ (instancetype) requestWithURLString: (NSString *) urlString
              requestMethod: (RequestMethod)requestMethod
            responseHandler: (RequestResponseHandler) responseHandler NS_RETURNS_NOT_RETAINED;

#pragma mark defaults
/*
 These class methods determine the behavior of NSURLRequest and could be
 overridden in subclasses.
 */
+ (NSURLRequestCachePolicy) defaultCachePolicy;
+ (NSTimeInterval) defaultTimeoutInterval;

#pragma mark methods
// Connection methods
- (void)startConnection;
- (void)cancelConnection;
- (void)setBodyParamsJSONSerialize:(NSDictionary *)params;

// Response handling methods
- (void) failWithError: (NSError *) error;
- (void) succeedWithResponse: (id) response;
- (void) preprocessResponse: (id) response;
@end
