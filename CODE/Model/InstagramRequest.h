//
//  InstagramRequest.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "URLRequest.h"

@interface InstagramRequest : URLRequest

#pragma mark class method-style initializers
+ (instancetype) requestWithURLString: (NSString *) urlString
                        requestMethod: (RequestMethod)requestMethod
                          useClientID: (BOOL)useClientID
                      responseHandler: (RequestResponseHandler) responseHandler NS_RETURNS_NOT_RETAINED;
#pragma mark initializer methods
- (instancetype)  initWithURLString: (NSString *) urlString
                      requestMethod: (RequestMethod)requestMethod
                        useClientID: (BOOL)useClientID
                    responseHandler: (RequestResponseHandler) responseHandler NS_RETURNS_RETAINED;

+ (NSString *)baseServerURL;
@end
