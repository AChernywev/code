//
//  ImageLoader.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "ImageLoader.h"

#import "DownloadingQueue.h"
#import "SDImageCache.h"

@interface ImageLoader()
//Dictionaty where for every url(NSString) key storing NSMutableSet of ImageLoaderCompletionBlock objects associated with this URL;
@property (nonatomic, strong) NSMutableDictionary *     loaderCompletionBlocks;

@property (nonatomic, strong) DownloadingQueue *        imageLoadingQueue;
@end

@implementation ImageLoader

#pragma mark - initialization
- (instancetype)init
{
    self = [super init];
    if(self) {
        self.loaderCompletionBlocks = [NSMutableDictionary dictionary];
        self.imageLoadingQueue = [[DownloadingQueue alloc]init];
        self.imageLoadingQueue.dataSource = (id<DownloadingQueueDataSource>)self;
        self.imageLoadingQueue.delegate = (id<DownloadingQueueDelegate>)self;
    }
    return self;
}

#pragma mark - public methods
- (void)loadImageFromURLString:(NSString *)urlString withCompletion:(ImageLoaderCompletionBlock)completion
{
    if(!urlString) {
        return;
    }
    
    if(completion) {
        NSMutableSet *urlBlocks = [_loaderCompletionBlocks objectForKey:urlString];
        if(!urlBlocks) {
            urlBlocks = [NSMutableSet setWithObject:[completion copy]];
        }
        else {
            [urlBlocks addObject:[completion copy]];
        }
        [_loaderCompletionBlocks setObject:urlBlocks forKey:urlString];
    }
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:urlString];
    if(!image) {
        [self.imageLoadingQueue addObjectToQueue:urlString];
    }
    else {
        [self finishWithImage:image forURLString:urlString];
    }
}

#pragma mark - working methods
- (void)finishWithImage:(UIImage *)image forURLString:(NSString *)urlString
{
    NSMutableSet *urlBlocks = [_loaderCompletionBlocks objectForKey:urlString];
    for(ImageLoaderCompletionBlock block in urlBlocks) {
        block(urlString, image);
    }
    [_loaderCompletionBlocks removeObjectForKey:urlString];
}

- (UIImage *)resizedImageFromImage:(UIImage *)responseImage
{
    CGSize size = CGSizeApplyAffineTransform([UIScreen mainScreen].bounds.size, CGAffineTransformMakeScale([UIScreen mainScreen].scale, [UIScreen mainScreen].scale));
    if(responseImage.size.width > responseImage.size.height) {
        CGFloat width = size.width;
        size.width = size.height;
        size.height = width;
    }
    
    CGSize (^aspectFitResizer)(CGSize originalImageSize, CGSize newImageSize) = ^CGSize(CGSize originalImageSize, CGSize newImageSize) {
        CGFloat widthFactor = originalImageSize.width / newImageSize.width;
        CGFloat heightFactor = originalImageSize.height / newImageSize.height;
        CGFloat factor = MAX(widthFactor, heightFactor);
        return CGSizeMake(originalImageSize.width / factor, originalImageSize.height / factor);
    };
    
    
    size = aspectFitResizer(responseImage.size, size);
    CGRect rect = CGRectZero;
    rect.size = size;
    
    
    CGRect realImageRect = CGRectZero;
    realImageRect.size = responseImage.size;
    
    UIImage *storingImage = nil;
    if(!CGRectContainsRect(rect, realImageRect)) {
        UIGraphicsBeginImageContext(size);
        [responseImage drawInRect:rect];
        storingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else {
        storingImage = responseImage;
    }
    return storingImage;
}

#pragma mark - <DownloadingQueueDataSource>
- (NSInteger)numberOfSimultaneousDownloads
{
    return 5;
}

- (NSString *)linkFromQueueObject:(id)object
{
    return object;
}

#pragma mark - <DownloadingQueueDelegate>
- (void)queue:(DownloadingQueue *)queue didFailedLoadObject:(id)object withError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self finishWithImage:nil forURLString:object];
    });
}

- (void)queue:(DownloadingQueue *)queue didLoadObject:(id)object withResponse:(id)response
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        UIImage *responseImage = [UIImage imageWithData:response];
        UIImage *storingImage = [self resizedImageFromImage:responseImage];
        [[SDImageCache sharedImageCache] storeImage:storingImage forKey:object];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self finishWithImage:storingImage forURLString:object];
        });
    });
}

@end
