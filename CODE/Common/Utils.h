//
//  Utils.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (void)showAlertWithMessage:(NSString *)message;
+ (void)showServerError:(NSError *)error;
+ (BOOL)validateUsername:(NSString *)username;
@end
