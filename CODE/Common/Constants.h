//
//  Common.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#define NULL_TO_NIL(obj)                            ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })
#define JSON_TO_BOOL(obj)                           ([((NSNumber *)NULL_TO_NIL(obj)) isKindOfClass:[NSNumber class]] ? ((NSNumber *)NULL_TO_NIL(obj)).boolValue : NO)
#define JSON_TO_INTEGER(obj)                        (((NSNumber *)NULL_TO_NIL(obj)).integerValue)
#define JSON_TO_FLOAT(obj)                          (((NSNumber *)NULL_TO_NIL(obj)).floatValue)
#define JSON_TO_DOUBLE(obj)                         (((NSNumber *)NULL_TO_NIL(obj)).doubleValue)
#define JSON_TO_STRING(obj)                         ((NSString *)NULL_TO_NIL(obj))
#define JSON_TO_DICTIONARY(obj)                     ([((NSDictionary *)NULL_TO_NIL(obj)) isKindOfClass:[NSDictionary class]] ? ((NSDictionary *)NULL_TO_NIL(obj)) : nil)
#define JSON_TO_ARRAY(obj)                          ([((NSArray *)NULL_TO_NIL(obj)) isKindOfClass:[NSArray class]] ? ((NSArray *)NULL_TO_NIL(obj)) : nil)