//
//  Utils.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (void)showServerError:(NSError *)error
{
    [self showAlertWithMessage:error.localizedDescription];
}

+ (void)showAlertWithMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

+ (BOOL)validateUsername:(NSString *)username
{
    NSString *regexpString = @"^[A-Za-z][A-Za-z0-9._]*$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regexpString];
    return [test evaluateWithObject:username];
}

@end
