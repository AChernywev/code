//
//  ImagePostsViewController.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/7/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "ImagePostsViewController.h"

#import "ImageCollectionViewCell.h"
#import "ProgressView.h"
#import "CollageViewController.h"

static NSInteger const kMaxSelectedItems = 10;

@interface ImagePostsViewController ()
@property (nonatomic, weak) IBOutlet UICollectionView * collectionView;
@property (nonatomic, weak) IBOutlet UILabel *          countLabel;
@property (nonatomic, weak) UIBarButtonItem *           nextButton;

@property (nonatomic, strong) NSMutableArray *          selectedImages;
@property (nonatomic, strong) NSArray *                 images;
@property (nonatomic, strong) User *                    user;
@property (nonatomic, strong) InstagramRequest *        currentRequest;

@end

@implementation ImagePostsViewController

#pragma mark - Actions
- (IBAction)nextButtonAction:(id)sender
{
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:self.selectedImages.count];
    for(NSNumber *number in self.selectedImages) {
        [objects addObject:[self.images objectAtIndex:number.integerValue]];
    }
    CollageViewController *collageController = [[CollageViewController alloc]initWithImages:objects];
    [self.navigationController pushViewController:collageController animated:YES];
}

#pragma mark - initialization
- (instancetype)initWithUser:(User *)user
{
    self = [super initWithNibName:nil bundle:nil];
    if(self) {
        _user = user;
        _selectedImages = [NSMutableArray array];
    }
    return self;
}

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.user.username;
    
    [self setupUI];
    [self updateImageInfo];
}

- (void)dealloc
{
    [self.currentRequest cancelConnection];
}

#pragma mark - loading methods

- (void)loadImagesInArray:(NSMutableArray *)imagesArray
               withNextID:(NSString *)nextID
          progressHandler:(void(^)(CGFloat progress))progressHandler
               completion:(void(^)(NSArray *images, NSError *error))completion
{
    __weak typeof(self) weakSelf = self;
    self.currentRequest = [self.user loadUserRecentImagesWithNextID:nextID
                                                         completion:^(NSArray *images, NSString *newNextID, NSError *error) {
                                                             weakSelf.currentRequest = nil;
                                                             if(!error) {
                                                                 [imagesArray addObjectsFromArray:images];
                                                                 CGFloat progress = (CGFloat)imagesArray.count / (CGFloat)weakSelf.user.postsCount;
                                                                 if(progressHandler) {
                                                                     progressHandler(progress);
                                                                 }
                                                                 if(newNextID.length) {
                                                                     [weakSelf loadImagesInArray:imagesArray withNextID:newNextID progressHandler:progressHandler completion:completion];
                                                                 }
                                                                 else {
                                                                     if(completion) {
                                                                         completion(imagesArray, nil);
                                                                     }
                                                                 }
                                                             }
                                                             else {
                                                                 if(completion) {
                                                                     completion(imagesArray, error);
                                                                 }
                                                             }
                                                         }
                           ];
}


- (void)updateImageInfo
{
    ProgressView *progressView = [[ProgressView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:progressView];
    
    self.busy = YES;
    __weak typeof(self) weakSelf = self;
    self.currentRequest = [self.user updateUserInfoWithCompletion:^(NSError *error) {
        weakSelf.currentRequest = nil;
        if(!error) {
            NSLog(@"Num Of Posts: %ld",(long)weakSelf.user.postsCount);
            NSMutableArray *imagesArray = [NSMutableArray array];
            [weakSelf loadImagesInArray:imagesArray
                             withNextID:nil
                        progressHandler:^(CGFloat progress) {
                            NSLog(@"Progress: %f", progress);
                            progressView.progress = progress;
                        }
                             completion:^(NSArray *images, NSError *error) {
                                 if(!error) {
                                     weakSelf.images = [images sortedArrayUsingComparator:^NSComparisonResult(Image *obj1, Image *obj2) {
                                         return [@(obj2.likesCount) compare:@(obj1.likesCount)];
                                     }];
                                     if(weakSelf.images.count > kMaxSelectedItems) {
                                         weakSelf.images = [weakSelf.images subarrayWithRange:NSMakeRange(0, kMaxSelectedItems)];
                                     }
                                     [weakSelf.collectionView reloadData];
                                     NSLog(@"Num Of Loaded Images: %lu",(unsigned long)images.count);
                                 }
                                 else {
                                     [Utils showServerError:error];
                                 }
                                 [progressView removeFromSuperview];
                                 weakSelf.busy = NO;
                             }
             ];
        }
        else {
            [progressView removeFromSuperview];
            weakSelf.busy = NO;
            [Utils showServerError:error];
        }
    }];
}

#pragma mark - working methods
- (void)setupUI
{
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kImageCellReuseIdentifier];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.backgroundView = [UIView new];
    self.nextButton = self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Далее" style:UIBarButtonItemStyleBordered target:self action:@selector(nextButtonAction:)];
    [self updateCountLabel];
}

- (void)updateCountLabel
{
    if(self.selectedImages.count) {
        self.nextButton.enabled = YES;
        self.countLabel.text = [NSString stringWithFormat:@"Выбрано %@ из %@ изображений",@(self.selectedImages.count), @(kMaxSelectedItems)];
    }
    else {
        self.nextButton.enabled = NO;
        self.countLabel.text = [NSString stringWithFormat:@"Выберите до %@ изображений", @(kMaxSelectedItems)];
    }
}

#pragma mark - <UICollectionViewDelegateFlowLayout> methods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(150.0f, 150.0f);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

#pragma mark - <UICollectionViewDataSource> methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kImageCellReuseIdentifier forIndexPath:indexPath];
    Image *image = [self.images objectAtIndex:indexPath.row];
    [cell setupCellForImage:image selected:[self.selectedImages containsObject:@(indexPath.row)]];
    return cell;
}

#pragma mark - <UICollectionViewDelegate> methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.selectedImages containsObject:@(indexPath.row)]) {
        [self.selectedImages removeObject:@(indexPath.row)];
    }
    else {
        if(self.selectedImages.count < kMaxSelectedItems) {
            [self.selectedImages addObject:@(indexPath.row)];
        }
    }
    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
    [self updateCountLabel];
}

@end
