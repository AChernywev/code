//
//  UserSearchViewController.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "UserSearchViewController.h"

#import "UserTableViewCell.h"
#import "ImagePostsViewController.h"

@interface UserSearchViewController ()
@property (nonatomic, weak)   IBOutlet UISearchBar *nameSearchBar;
@property (nonatomic, weak)   IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *             users;
@end

@implementation UserSearchViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customizeUI];
}

#pragma mark - working methods
- (void)customizeUI
{
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([UserTableViewCell class]) bundle:nil] forCellReuseIdentifier:kUserTableCellReuseIdentifier];
}

#pragma mark - loading methods
- (void)updateUsersWithName:(NSString *)username
{
    self.busy = YES;
    __weak typeof(self) weakSelf = self;
    [User searchUsersWithString:username completion:^(NSArray *users, NSError *error) {
        if(!error) {
            weakSelf.users = users;
            [weakSelf.tableView reloadData];
        }
        else {
            [Utils showServerError:error];
        }
        weakSelf.busy = NO;
    }];
}

#pragma mark - <UISearchBarDelegate> method
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    if(searchBar.text.length > 0) {
        if([Utils validateUsername:searchBar.text]) {
            [self updateUsersWithName:self.nameSearchBar.text];
        }
        else {
            [Utils showAlertWithMessage:@"Неверное имя пользователя"];
        }
    }
}

#pragma mark - <UITableViewDataSource> methods
- (NSInteger)numberOfSectionInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.users.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    User *user = [self.users objectAtIndex:indexPath.row];
    return [UserTableViewCell cellHeightForBio:user.bio];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kUserTableCellReuseIdentifier];
    User *user = [self.users objectAtIndex:indexPath.row];
    [cell setupCellForUser:user];
    return cell;
}

#pragma mark - <UITableViewDelegate> methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    User *user = [self.users objectAtIndex:indexPath.row];
    ImagePostsViewController *postsController = [[ImagePostsViewController alloc]initWithUser:user];
    [self.navigationController pushViewController:postsController animated:YES];
}

@end
