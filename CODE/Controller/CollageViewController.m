//
//  CollageViewController.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/8/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "CollageViewController.h"

#import "UIImageView+Remote.h"
#import "PhotoPageRenderer.h"

static CGFloat const kDefaultImageSize = 300.0f;

@interface CollageViewController ()
@property (nonatomic, weak) IBOutlet UIImageView *  collageImageView;

@property (nonatomic, strong) NSArray *             images;
@property (nonatomic, weak) UIBarButtonItem *       printButton;

@end

@implementation CollageViewController

#pragma mark - Actions
- (IBAction)printButtonAction:(id)sender
{
    UIPrintInteractionController *controller = [UIPrintInteractionController sharedPrintController];
    
    UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if(completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
        }
    };
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    UIImage *image = self.collageImageView.image;
    printInfo.outputType = UIPrintInfoOutputPhoto;
    printInfo.jobName = self.title;
    
    if(image.size.width > image.size.height) {
        printInfo.orientation = UIPrintInfoOrientationLandscape;
    }
    else {
        printInfo.orientation = UIPrintInfoOrientationPortrait;
    }
    controller.printInfo = printInfo;
    
    PhotoPageRenderer *pageRenderer = [[PhotoPageRenderer alloc]initWithImage:image];
    controller.printPageRenderer = pageRenderer;
    
    [controller presentAnimated:YES completionHandler:completionHandler];
}

#pragma mark - initialization
- (instancetype)initWithImages:(NSArray *)images
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _images = images;
    }
    return self;
}

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Коллаж";
    self.printButton = self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Печать" style:UIBarButtonItemStyleBordered target:self action:@selector(printButtonAction:)];
    self.printButton.enabled = NO;
    
    self.busy = YES;
    [self drawImages:self.images withCompletion:^(UIImage *resultImage) {
        self.printButton.enabled = YES;
        self.collageImageView.image = resultImage;
        self.busy = NO;
    }];
}

#pragma mark - working methods
- (void)drawImages:(NSArray *)images withCompletion:(void(^)(UIImage *resultImage))completion
{
    CGSize size = [self sizeForImagesCount:images.count];
    UIGraphicsBeginImageContext(size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    [self drawImageAtIndex:0
                fromImages:images
                 inContext:ctx
            withCompletion:^{
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                if(completion) {
                    completion(image);
                }
            }
     ];
}

- (void)drawImageAtIndex:(NSInteger)index
                   fromImages:(NSArray *)images
                    inContext:(CGContextRef)ctx
               withCompletion:(dispatch_block_t)completion
{
    if(index < images.count) {
        Image *imageObject = [images objectAtIndex:index];
        [[Model shared].imageLoader loadImageFromURLString:imageObject.lowResolutionURLString withCompletion:^(NSString *imageURLString, UIImage *image) {
            if([imageURLString isEqualToString:imageObject.lowResolutionURLString]) {
                CGRect drawingRect = [self rectForIndex:index forImagesCount:images.count];
                [image drawInRect:drawingRect];
                
                NSInteger nextIndex = index + 1;
                [self drawImageAtIndex:nextIndex fromImages:images inContext:ctx withCompletion:completion];
            }
        }];
    }
    else {
        if(completion) {
            completion();
        }
    }
}

#pragma mark - support methods
- ( NSInteger)maxItemsInRowForImagesCount:(NSInteger)count
{
    switch (count) {
        case 0: {
            return 0;
        }
            break;
        case 1: {
            return 1;
        }
            break;
        case 2: {
            return 2;
        }
            break;
        case 3: {
            return 3;
        }
            break;
        case 4: {
            return 2;
        }
            break;
        case 5: {
            return 3;
        }
            break;
        case 6: {
            return 3;
        }
            break;
        case 7: {
            return 4;
        }
            break;
        case 8: {
            return 3;
        }
            break;
        case 9: {
            return 3;
        }
            break;
        case 10: {
            return 4;
        }
            break;
            
        default: {
            NSAssert(NO, @"To Many Image Selected");
            return 0;
        }
            break;
    }
}

- (CGSize)sizeForImagesCount:(NSInteger)count
{
    NSInteger numOfObjectsInRow = [self maxItemsInRowForImagesCount:count];
    NSInteger fullRowsCount = (count / numOfObjectsInRow) + (count % numOfObjectsInRow != 0);
    return CGSizeMake(kDefaultImageSize * numOfObjectsInRow, fullRowsCount * kDefaultImageSize);
}

- (CGRect)rectForIndex:(NSInteger)index forImagesCount:(NSInteger)count
{
    NSInteger numOfObjectsInRow = [self maxItemsInRowForImagesCount:count];
    NSInteger fullRowsCount = (count / numOfObjectsInRow);
    NSInteger rowIndex = index / numOfObjectsInRow;
    NSInteger columnIndex = index % numOfObjectsInRow;
    if(rowIndex < fullRowsCount) {
        return CGRectMake(columnIndex * kDefaultImageSize, rowIndex * kDefaultImageSize, kDefaultImageSize, kDefaultImageSize);
    }
    else {
        NSInteger remainsCount = (count % numOfObjectsInRow);
        NSInteger numOfGaps = remainsCount + 1;
        CGSize size = [self sizeForImagesCount:count];
        CGFloat gapWidth = (size.width - kDefaultImageSize * remainsCount) / numOfGaps;
        return CGRectMake(columnIndex * kDefaultImageSize + gapWidth * (columnIndex + 1), rowIndex * kDefaultImageSize, kDefaultImageSize, kDefaultImageSize);
    }
}

#pragma mark - <UIScrollViewDelegate> methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.collageImageView;
}

#pragma mark - IOS 6 rotation support
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

@end
