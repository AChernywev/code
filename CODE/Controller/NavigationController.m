//
//  NavigationController.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "NavigationController.h"

@implementation NavigationController

#pragma mark - Container logic
- (BOOL)shouldAutomaticallyForwardAppearanceMethods
{
    return [self.topViewController shouldAutomaticallyForwardAppearanceMethods];
}

- (BOOL)shouldAutomaticallyForwardRotationMethods
{
    return [self.topViewController shouldAutomaticallyForwardRotationMethods];
}

#pragma mark - IOS 6 rotation support
- (NSUInteger)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}

- (BOOL)shouldAutorotate
{
    return [self.topViewController shouldAutorotate];;
}

#pragma mark - Status Bar Logic
- (BOOL)prefersStatusBarHidden
{
    return [self.topViewController prefersStatusBarHidden];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return [self.topViewController preferredStatusBarStyle];
}

#pragma mark - controller content size
- (UIRectEdge)edgesForExtendedLayout
{
    return [self.topViewController edgesForExtendedLayout];
}

@end
