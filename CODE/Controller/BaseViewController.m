//
//  BaseViewController.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "BaseViewController.h"

#import "ActivityIndicatorView.h"

@interface BaseViewController ()
@property (nonatomic, strong) ActivityIndicatorView * activityView;

@end

@implementation BaseViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    if(!self.activityView.superview) {
        [self.view addSubview:self.activityView];
    }
    [self hideActivityView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.busy) {
        [self showActivityView];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideActivityView];
}

#pragma mark - properties
- (void)setBusy:(BOOL)busy
{
    if(![NSThread isMainThread]) {
        [self performSelector:@selector(setBusy:) withObject:@(busy) afterDelay:NO];
        return ;
    }
    if(_busy != busy) {
        _busy = busy;
        if(_busy) {
            [self showActivityView];
        }
        else {
            [self hideActivityView];
        }
    }
}

- (BOOL)isBusy
{
    return _busy;
}

- (ActivityIndicatorView *)activityView
{
    if(!_activityView) {
        _activityView = [[ActivityIndicatorView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _activityView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _activityView;
}

#pragma mark - Activity Logic
- (void)showActivityView
{
    [self.activityView.superview bringSubviewToFront:self.activityView];
    [self.activityView startActivity:self.view];
    self.view.userInteractionEnabled = NO;
}

- (void)hideActivityView
{
    [self.activityView finishActivity];
    self.view.userInteractionEnabled = YES;
}

#pragma mark - Container logic
- (BOOL)shouldAutomaticallyForwardAppearanceMethods
{
    return YES;
}

- (BOOL)shouldAutomaticallyForwardRotationMethods
{
    return YES;
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    if([parent isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navController = (UINavigationController *)parent;
        [self.activityView removeFromSuperview];
        [navController.navigationBar.superview addSubview:self.activityView];
    }
}

#pragma mark - IOS 6 rotation support
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

#pragma mark - Status Bar Logic
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

#pragma mark - controller content size
- (UIRectEdge)edgesForExtendedLayout
{
    return UIRectEdgeNone;
}

@end
