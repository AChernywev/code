//
//  ImagePostsViewController.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/7/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "BaseViewController.h"

@interface ImagePostsViewController : BaseViewController

- (instancetype)initWithUser:(User *)user;
@end
