//
//  CollageViewController.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/8/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "BaseViewController.h"

@interface CollageViewController : BaseViewController

- (instancetype)initWithImages:(NSArray *)images;
@end
