//
//  BaseViewController.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

@interface BaseViewController : UIViewController
{
    BOOL            _busy;
}
@property (nonatomic, assign, getter = isBusy) BOOL busy;

@end
