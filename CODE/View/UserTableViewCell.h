//
//  UserTableViewCell.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/8/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

static NSString * const kUserTableCellReuseIdentifier = @"kUserTableCellReuseIdentifier";

@interface UserTableViewCell : UITableViewCell

+ (CGFloat)cellHeightForBio:(NSString *)bio;
- (void)setupCellForUser:(User *)user;
@end
