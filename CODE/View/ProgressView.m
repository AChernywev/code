//
//  ProgressView.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/7/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "ProgressView.h"

@interface ProgressView ()
@property (nonatomic, weak) UIActivityIndicatorView *   activityIndicatorView;
@property (nonatomic, weak) UIProgressView *            progressView;
@property (nonatomic, weak) UIView *                    blackOverlayView;

@end

@implementation ProgressView

#pragma mark - initialization
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setupView];
    }
    return self;
}

- (void)setupView
{
    self.backgroundColor = [UIColor clearColor];
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.userInteractionEnabled = NO;
    
    UIView *blackView = [[UIView alloc]initWithFrame:self.bounds];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.6f;
    [self addSubview:blackView];
    self.blackOverlayView = blackView;
    
    UIActivityIndicatorView *actIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self addSubview:actIndicatorView];
    self.activityIndicatorView = actIndicatorView;
    
    UIProgressView *progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
    progressView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self addSubview:progressView];
    self.progressView = progressView;
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark - properties
- (void)setProgress:(CGFloat)progress
{
    self.progressView.progress = progress;
}

- (CGFloat)progress
{
    return self.progressView.progress;
}

#pragma mark - UIView methods
- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    self.frame = newSuperview.bounds;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.blackOverlayView.frame = self.bounds;
    
    CGRect progressFrame = CGRectZero;
    progressFrame.size.width = self.bounds.size.width - 20;
    progressFrame.size.height = 10.0f;
    self.progressView.frame = progressFrame;
    self.progressView.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds) + 30);
    
    self.activityIndicatorView.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds) - 30);
}

@end
