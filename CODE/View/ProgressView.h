//
//  ProgressView.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/7/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView
@property (nonatomic, assign) CGFloat progress;

@end
