//
//  UserTableViewCell.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/8/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "UserTableViewCell.h"

#import "UIImageView+Remote.h"

@interface UserTableViewCell()
@property (nonatomic, weak) IBOutlet UILabel *      usernameLabel;
@property (nonatomic, weak) IBOutlet UILabel *      fullNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *      bioLabel;
@property (nonatomic, weak) IBOutlet UIImageView *  avatarView;

@end

@implementation UserTableViewCell

#pragma mark - Class methods
+ (CGFloat)bioWidth
{
    return 275.0f;
}

+ (CGFloat)bioTopOffset
{
    return 45.0f;
}

+ (CGFloat)bioBottomOffset
{
    return 5.0f;
}

+ (UIFont *)bioFont;
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
}

+ (CGFloat)heightForBio:(NSString *)bio
{
    NSDictionary *attributes = @{NSFontAttributeName:[[self class] bioFont]};
    return [bio boundingRectWithSize:CGSizeMake([[self class] bioWidth], MAXFLOAT)
                             options:NSStringDrawingUsesLineFragmentOrigin
                          attributes:attributes
                             context:nil].size.height;
}

+ (CGFloat)cellHeightForBio:(NSString *)bio
{
    if(bio.length) {
        return [[self class] heightForBio:bio] + [[self class] bioTopOffset] + [[self class] bioBottomOffset];
    }
    else {
        return [[self class] bioTopOffset];
    }
}

#pragma mark - lifecycle
- (void)awakeFromNib
{
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView = [UIView new];
}

#pragma mark - public methods
- (void)setupCellForUser:(User *)user
{
    self.usernameLabel.text = user.username;
    self.fullNameLabel.text = user.fullname;
    self.bioLabel.text = user.bio;
    [self.avatarView setImageWithURLString:user.profilePictureURLString withActivity:YES defaultImage:nil];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark - UITableViewCell methods
- (void)prepareForReuse
{
    [super prepareForReuse];
    self.usernameLabel.text = nil;
    self.fullNameLabel.text = nil;
    self.bioLabel.text = nil;
    self.avatarView.image = nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect bioFrame = self.bioLabel.frame;
    bioFrame.origin.y = [[self class] bioTopOffset];
    bioFrame.size.width = [[self class] bioWidth];
    bioFrame.size.height = [[self class] heightForBio:self.bioLabel.text];
    self.bioLabel.frame = bioFrame;
}
@end
