//
//  PhotoCollectionViewCell.h
//  ANGRYCITIZEN
//
//  Created by Alexandr Chernyshev on 21.01.14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

static NSString * const kImageCellReuseIdentifier = @"ImageCellReuseIdentifier";

@interface ImageCollectionViewCell : UICollectionViewCell

- (void)setupCellForImage:(Image *)image selected:(BOOL)selected;
@end
