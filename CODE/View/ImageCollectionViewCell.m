//
//  PhotoCollectionViewCell.m
//  ANGRYCITIZEN
//
//  Created by Alexandr Chernyshev on 21.01.14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "ImageCollectionViewCell.h"

#import "UIImageView+Remote.h"

@interface ImageCollectionViewCell ()
@property (nonatomic, weak) IBOutlet UIImageView * imageView;
@property (nonatomic, weak) IBOutlet UIImageView * selectionImageView;
@property (nonatomic, weak) IBOutlet UILabel *     likeLabel;

@end

@implementation ImageCollectionViewCell

#pragma mark - public methods
- (void)setupCellForImage:(Image *)image selected:(BOOL)selected
{
    [self.imageView setImageWithURLString:image.lowResolutionURLString withActivity:YES defaultImage:nil];
    self.likeLabel.text = [NSString stringWithFormat:@"%@ likes", @(image.likesCount)];
    self.selectionImageView.hidden = !selected;
}

#pragma mark - UICollectionView methods
- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imageView.image = nil;
    self.likeLabel.text = nil;
    self.selectionImageView.hidden = NO;
}

@end
