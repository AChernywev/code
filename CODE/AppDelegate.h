//
//  AppDelegate.h
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
